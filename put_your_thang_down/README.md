## Put your thang down flip it and reverse it

* **Category:** rev
* **Points:** 150
* **Author(s):** b0uldrr
* **Date:** 12/04/20

---

### Challenge
```
Ra-ta-ta-ta-ta-ta-ta-ta-ta-ta.

Authors: trashcanna and pleoxconfusa
```

### Downloads
* [missyelliott](missyelliott)

---

### Solution

Running the provided binary, it prompts for user input, examines the input and then states that the input was wrong and exits. We need to find the right input.

```
$ ./missyelliott 
Let me search ya.
test input
Wrong. You need to work it.
```

I opened the binary in Ghidra. The binary has been stripped, so none of the functions or variables have meaningful names. I spent a bit of time going through every function, renaming functions and variables, trying to understand what was happening. In the end, I found there were only 4 important functions, which I named `start`, `do_bitwise_not`, `do_stuff` and `check_input`:

![start](images/start.png)

![do_bitwise_not](images/do_bitwise_not.png)

![do_stuff](images/do_stuff.png)

![check_input](images/check_input.png)

Essentially, all the program does is:
1. Prompt for user input
1. Check that user input is 43 bytes in length (`start`, line 10)
1. Perform a bitwise not on every byte of the user input (`do_bitwise_not` function)
1. Do a whole bunch of funky bit operations on each character of the user input (`do_stuff`, lines 12 - 22)
1. Reverse the order of the user input bytes (`do_stuff`, lines 24 - 29)
1. Check the value of the now changed user input against another stored value - assumed to be the encrypted flag (`check_input`, line 7)
1. Tell the user if their input matches this other value and then exit

I assumed that the stored value was an encrypted representation of the flag and that we would need to enter the unencrypted flag to win.

My plan of attack for this challenge was:
1. Do a live analysis of the binary using GDB to find out the stored encrypted flag value
1. Recreate steps 3, 4 and 5 above in python
1. Run every printable character through that algorithm and store its plaintext value and encrypted value in a dictionary
1. Check the value of each byte of the encrypted flag against our dictionary to decrypt the flag

The hardest part of this challenge was finding a place to put a put a breakpoint in the binary in GDB. Because it had been stripped, I couldn't just open GDB and disassemble "start" or "check\_input" because those were function names that I made up. I knew that I wanted to put my breakpoint at line 7 in `check_input` because that's when the encrypted flag would be stored in the RSI register. The corresponding line in an objdump is:

```
11d5:       e8 66 fe ff ff          callq  1040 <strncmp@plt>
```

In the end, I opened the binary in GDB and ran it. When it prompted me for input, I pressed ctrl-c to break execution. I was now inside the `fgets` call on line 8 of the `start` function - so I was inside libc somewhere. I keep submitting `finish` commands, which return to the end of the current stack frame, until I finally returned from libc into the `start` function, on line 10. Alternatively I could have kept submitting `next` commands (`n`) but this would have taken much longer, because I would be stepping through every command.

Now that I was back inside `start`, I could see that I was at address 0x5555555553b3 and I knew the offset of our address location by comparing it to the address of the same location using `objdump`. I set a breakpoint at my desired location as stated above, using:

```
gdb-peda$ b *0x5555555551d5
Breakpoint 1 at 0x5555555551d5
```

Now that I have a breakpoint where I will be able to examine the encrypted flag, I started the program again, making sure that my submitted input was exactly 43 bytes so as to pass the first if gate on line 10 of `start`. To get 43 characters I used an in-line python script:

```
$ python -c "print('a'*43)"
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
```

Starting the program in GDB and submitting my input:
```
gdb-peda$ r
Starting program: /home/tmp/ctf/dawgctf/put_your_thang_down/missyelliott 
Let me search ya.
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
```

At the breakpoint, I examined 43 bytes in the RSI register. This is our encrypted flag and what we need to reverse:
```
gdb-peda$ x/43bx $rsi
0x555555556008: 0x41    0xf5    0x51    0xd1    0x4d    0x61    0xd5    0xe9
0x555555556010: 0x69    0x89    0x19    0xdd    0x09    0x11    0x89    0xcb
0x555555556018: 0x9d    0xc9    0x69    0xf1    0x6d    0xd1    0x7d    0x89
0x555555556020: 0xd9    0xb5    0x59    0x91    0x59    0xb1    0x31    0x59
0x555555556028: 0x6d    0xd1    0x8b    0x21    0x9d    0xd5    0x3d    0x19
0x555555556030: 0x11    0x79    0xdd
```

As an aside, I could also see see 43 'a' characters I inputted, held in the RDI register. The algorithm changed their normal 0x61 hex value to 0x79:
```
gdb-peda$ x/43bx $rdi
0x555555558040: 0x79    0x79    0x79    0x79    0x79    0x79    0x79    0x79
0x555555558048: 0x79    0x79    0x79    0x79    0x79    0x79    0x79    0x79
0x555555558050: 0x79    0x79    0x79    0x79    0x79    0x79    0x79    0x79
0x555555558058: 0x79    0x79    0x79    0x79    0x79    0x79    0x79    0x79
0x555555558060: 0x79    0x79    0x79    0x79    0x79    0x79    0x79    0x79
0x555555558068: 0x79    0x79    0x79
```

I copied the encrypted flag values into a python script and then recreated the encryption algorithm. I ran every printable character through the algorithm and made a dictionary of plaintext -> encrypted text pairs. Once that was complete, I compared the encrypted flag values against the dictionary to find their plaintext counterparts. This gave the flag. Here is my script:

```python
#! /usr/bin/python3
import pwn
import string

# This is our encrypted flag, found by examning the program while it was running in GDB
flag = [0x41, 0xf5, 0x51, 0xd1, 0x4d, 0x61, 0xd5, 0xe9, 0x69, 0x89, 0x19, 0xdd, 0x09, 0x11, 0x89, 0xcb, 0x9d, 0xc9, 0x69, 0xf1, 0x6d, 0xd1, 0x7d, 0x89, 0xd9, 0xb5, 0x59, 0x91, 0x59, 0xb1, 0x31, 0x59, 0x6d, 0xd1, 0x8b, 0x21, 0x9d, 0xd5, 0x3d, 0x19, 0x11, 0x79, 0xdd]

# Initialise a dictionary to store our encrypted chars with their plaintext chars
encrypted_mapping = {}

# Loop through every printable char and run it through the encyprtion algorithm found in the missyelliott binary
# We don't really have to understand why this algorithm does what it does, we just need to make sure it does it the same as the original binary so that we get the same output
for char in string.printable:

    # bitwise not every character (this is what do_bitwise_not() does)
    not_char = ~ ord(char)

    # do all of the other funky stuff that do_stuff() does
    output = 0
    bit_counter = 0
    while(bit_counter < 8): 
            if ((1 << (bit_counter & 0x1f)) & not_char) != 0:
                output = output | (1 << (7 - (bit_counter & 0x1f)))
            bit_counter += 1

    # plaintext & encrypted text in a dictionary
    encrypted_mapping[hex(output)] = char

    # Debug printing
    print(char, ":", hex(output))

# The flag is reversed in do_stuff(). Undo that
flag.reverse()

# Now go through every encrypted value in the flag list, find its corresponding plaintext char in the dictionary we just created, and print it to screen
for i in flag:
    print(encrypted_mapping[hex(i)],end='')
print()
```

### Flag 
```
DawgCTF{.tIesreveRdnAtIpilF,nwoDgnihTyMtuP}
```
