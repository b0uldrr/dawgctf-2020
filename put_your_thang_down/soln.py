#! /usr/bin/python3
import pwn
import string

# This is our encrypted flag, found by examning the program while it was running in GDB
flag = [0x41, 0xf5, 0x51, 0xd1, 0x4d, 0x61, 0xd5, 0xe9, 0x69, 0x89, 0x19, 0xdd, 0x09, 0x11, 0x89, 0xcb, 0x9d, 0xc9, 0x69, 0xf1, 0x6d, 0xd1, 0x7d, 0x89, 0xd9, 0xb5, 0x59, 0x91, 0x59, 0xb1, 0x31, 0x59, 0x6d, 0xd1, 0x8b, 0x21, 0x9d, 0xd5, 0x3d, 0x19, 0x11, 0x79, 0xdd]

# Initialise a dictionary to store our encrypted chars with their plaintext chars
encrypted_mapping = {}

# Loop through every printable char and run it through the encyprtion algorithm found in the missyelliott binary
# We don't really have to understand why this algorithm does what it does, we just need to make sure it does it the same as the original binary so that we get the same output
for char in string.printable:
    not_char = ~ ord(char)
    output = 0
    bit_counter = 0
    while(bit_counter < 8): 
            if ((1 << (bit_counter & 0x1f)) & not_char) != 0:
                output = output | (1 << (7 - (bit_counter & 0x1f)))
            bit_counter += 1
    encrypted_mapping[hex(output)] = char

    # Debug printing
    print(char, ":", hex(output))

flag.reverse()

# Now go through every encrypted value in the flag list, find its corresponding plaintext char in the dictionary we just created, and print it to screen
for i in flag:
    print(encrypted_mapping[hex(i)],end='')
print()

