## DawgCTF 2020

* **CTF Time page:** https://ctftime.org/event/1030
* **Category:** Jeopardy
* **Date:** Fri, 10 April 2020, 22:00 UTC — Sun, 12 April 2020, 22:00 UTC

---

### Solved Challenges
| Name                                     | Category | Points | Key Concepts |
|------------------------------------------|----------|--------|--------------|
|bof to the top                            |pwn       |100     |bof, 32 bit, parameters|
|miracle mile                              |code      |100     |q&a coding|
|on lockdown                               |pwn       |50      |bof|
|put your thang down flip it and reverse it|rev       |150     |static analysis, live analysis|
|tom nook the capialist racoon             |pwn       |200     | |


### Unsolved Challenges to Revisit
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|where we roppin boys        |pwn       |        |bof, mmap(), rop|
