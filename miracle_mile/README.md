## Miracle Mile

* **Category:** coding
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 11/04/20

---

### Challenge
```
You didn't think I'd graduate without writing a running themed challenge, did you?

nc ctf.umbccd.io 5300

Note: pace is measured in minutes/mile (i.e. 10 miles in 1:40:00 is 10:00 minutes/mile)

Author: trashcanna
```
---

### Solution
Connecting to the remote sever, the challenge is to solve pace given a distance and time. You only have a second or so to solve the challenge before it times out.

```
$ nc ctf.umbccd.io 5300
-----------------------------------------
Hi, I'm Anna and I really like running
I'm too broke to get a gps watch though :(
Think you can figure out my average pace?
-----------------------------------------
I ran 18 in 2:22:03 What's my pace? 
Ha too slow!
```

I wrote a python script to automate the process.

```python
#! /usr/bin/python3
import pwn
from math import *

conn = pwn.remote("ctf.umbccd.io", 5300)

def solve(distance, time):

    # Convert our HH:MM:SS to seconds
    time  = time.split(':')
    hours = int(time[0])
    mins  = int(time[1])
    secs  = int(time[2])
    total_secs     = (hours * 3600) + (mins * 60) + secs

    # Work out seconds per mile
    secs_per_mile  = total_secs / float(distance) 

    # Convert seconds per mile to MM:SS
    mins_per_mile  = floor(secs_per_mile / 60)
    remainder_secs = floor(secs_per_mile % 60)

    return str(mins_per_mile) + ":" + str(remainder_secs)

# Receive the first few lines of the challenge description
for i in range(0,5):
    print(conn.recvline().decode())

# Keep looping, receving and solving challenges until we find the line that says "Dang you're pretty quick"
while(True):
    challenge = conn.recvline()
    print(challenge.decode())

    # If the line reads "Dang you're pretty quick" then print the flag and quit
    if "Dang" in challenge.decode():
        print(conn.recvall().decode())
        quit()

    # Else, split up our challenge line to get the time and distance
    challenge = challenge.split()
    distance = challenge[2].decode()
    time = challenge[4].decode()

    # Solve the challenge
    answer = solve(distance, time)
    print(answer)
    conn.sendline(answer)
```

After 389 challenge questions, we finally get the flag.

```
$ ./soln.py 
[+] Opening connection to ctf.umbccd.io on port 5300: Done
-----------------------------------------

Hi, I'm Anna and I really like running

I'm too broke to get a gps watch though :(

Think you can figure out my average pace?

-----------------------------------------

I ran 18 in 2:22:03 What's my pace? 

7:53
I ran 11 in 1:40:13 What's my pace? 

9:6
.
.
.
I ran 5 in 0:42:00 What's my pace? 

8:24
I ran 18 in 2:20:42 What's my pace? 

7:49
I ran 12 in 1:40:00 What's my pace? 

8:20
Dang you're pretty quick

[+] Receiving all data: Done (38B)
[*] Closed connection to ctf.umbccd.io port 5300
flag: DawgCTF{doe5n't_ruNN1ng_sUcK?!}
```

### Flag 
```
DawgCTF{doe5n't_ruNN1ng_sUcK?!}
```
