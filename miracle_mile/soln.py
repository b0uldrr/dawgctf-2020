#! /usr/bin/python3
import pwn
from math import *

conn = pwn.remote("ctf.umbccd.io", 5300)

def solve(distance, time):

    # Convert our HH:MM:SS to seconds
    time  = time.split(':')
    hours = int(time[0])
    mins  = int(time[1])
    secs  = int(time[2])
    total_secs     = (hours * 3600) + (mins * 60) + secs

    # Work out seconds per mile
    secs_per_mile  = total_secs / float(distance) 

    # Convert seconds per mile to MM:SS
    mins_per_mile  = floor(secs_per_mile / 60)
    remainder_secs = floor(secs_per_mile % 60)

    return str(mins_per_mile) + ":" + str(remainder_secs)


# Receive the first few lines of the challenge description
for i in range(0,5):
    print(conn.recvline().decode())

# Keep looping, receving and solving challenges until we find the line that says "Dang you're pretty quick"
while(True):
    challenge = conn.recvline()
    print(challenge.decode())

    # If the line reads "Dang you're pretty quick" then print the flag and quit
    if "Dang" in challenge.decode():
        print(conn.recvall().decode())
        quit()

    # Else, split up our challenge line to get the time and distance
    challenge = challenge.split()
    distance = challenge[2].decode()
    time = challenge[4].decode()

    # Solve the challenge
    answer = solve(distance, time)
    print(answer)
    conn.sendline(answer)
