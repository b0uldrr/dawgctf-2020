## Tom Nook the Capitalist Racoon 

* **Category:** pwn 
* **Points:** 200
* **Author(s):** b0uldrr
* **Date:** 13/04/20

---

### Challenge
```
Anyone else hear about that cool infinite bell glitch?

nc ctf.umbccd.io 4400

Author: trashcanna

```

### Downloads
* [animal_crossing](animal_crossing)

---

### Solution

Running the provided binary, we are presented with a shop where we can buy and sell things:

![screenshot](images/screenshot.png)

We start with an initial dollar value ("bells") of 8500. We can buy the flag in the shop for 420000.

After playing around with the program for a bit, I found that you could buy an item from the shop and sell it back to them. You would make money from the sale but the item would stay in your inventory. It was just a matter of selling the item over and over and over until reaching a bank value of 420000 when we could buy the flag. I made a script to automate the process. There's a little bit of setup to begin with, because you have to sell a few things from your inventory first to make space and then it's worth buying the tarantula from the shop because this sells for the most value (8000 bells). Here's my script ([soln](soln.py)):

```python
#! /usr/bin/python3
import pwn

local = True

if local:
    conn = pwn.process("./animal_crossing")
else:
    conn = pwn.remote("ctf.umbccd.io", 4400)

# Starting money
money = 8500

def initial_setup():
    global money

    # Select the sell menu
    conn.recvuntil("Choice: ")
    conn.sendline("1")                              

    for i in range(0, 6):
        conn.recvline()

    # Sell the flimsy axe
    conn.sendline("1")
    money += 400

    # Select the sell menu again
    conn.recvuntil("Choice: ")
    conn.sendline("1")

    for i in range(0, 5):
        conn.recvline().decode()

    # Sell the flimsy shovel
    conn.sendline("4")
    money += 800

    # Select the buy menu 
    conn.recvuntil("Choice: ").decode()
    conn.sendline("2")

    for i in range(0, 9):
        conn.recvline().decode()

    # Buy the tarantula
    conn.sendline("2")
    money -= 8000


def sell_loop():
    global money

    # Select the sell menu
    conn.recvuntil("Choice: ")
    conn.sendline("1")
    conn.recvuntil("900 bells\n")

    # Sell the tarantula
    conn.sendline("1")
    money += 8000
    print("Money:", money)

initial_setup()
while money < 420000:
    sell_loop()
conn.interactive()
```
And the output...

```
$ ./soln.py 
[+] Opening connection to ctf.umbccd.io on port 4400: Done
Money: 9700
Money: 17700
Money: 25700
Money: 33700
Money: 41700
Money: 49700
Money: 57700
Money: 65700
Money: 73700
Money: 81700
Money: 89700
Money: 97700
Money: 105700
Money: 113700
Money: 121700
Money: 129700
Money: 137700
Money: 145700
Money: 153700
Money: 161700
Money: 169700
Money: 177700
Money: 185700
Money: 193700
Money: 201700
Money: 209700
Money: 217700
Money: 225700
Money: 233700
Money: 241700
Money: 249700
Money: 257700
Money: 265700
Money: 273700
Money: 281700
Money: 289700
Money: 297700
Money: 305700
Money: 313700
Money: 321700
Money: 329700
Money: 337700
Money: 345700
Money: 353700
Money: 361700
Money: 369700
Money: 377700
Money: 385700
Money: 393700
Money: 401700
Money: 409700
Money: 417700
Money: 425700
[*] Switching to interactive mode

Timmy: A tarantula!
Sure! How about if I offer you
8000 Bells?
Thank you! Please come again!

1. I want to sell
2. What's for sale?
3. See you later.
Choice: $ 2

426100 bells
Timmy: Here's what we have to sell today.
\x00\x00\x00\x00\x00\x00\x00\x00. flimsy net - 400 bells
2. tarantula - 8000 bells
3. slingshot - 900 bells
4. sapling - 640 bells
5. cherry - 400 bells
6. flag - 420000 bells
$ 6

Timmy: Excellent purchase!
Yes, thank you for the bells
1. I want to sell
2. What's for sale?
3. See you later.
Choice: $ 1

Of course! What exactly are you
offering?
1. tarantula - I hate spiders! Price: 8000 bells
2. olive flounder - it's looking at me funny Price: 800 bells
3. slingshot - the closest thing you can get to a gun Price: 900 bells
4. flag - DawgCTF{1nf1n1t3_t@rantul@$} Price: 420000 bells
```

### Flag 
```
DawgCTF{1nf1n1t3_t@rantul@$}
```
